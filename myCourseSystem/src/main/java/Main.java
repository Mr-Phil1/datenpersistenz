import dataAccess.Course.MySQLCourseRepository;
import dataAccess.Student.MySQLStudentRepository;
import ui.Cli;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        Cli myCli = null;
        try {
            myCli = new Cli(new MySQLCourseRepository(), new MySQLStudentRepository());
            myCli.start();
        } catch (SQLException e) {
            System.out.println("Datenbankfehler: " + e.getMessage() + " SQL State: " + e.getSQLState());
        } catch (ClassNotFoundException e) {
            System.out.println("Datenbankfehler: " + e.getMessage());
        }
    }
}
