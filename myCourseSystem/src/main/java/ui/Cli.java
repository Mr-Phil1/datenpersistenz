package ui;

import dataAccess.DatabaseException;
import dataAccess.Course.MyCourseRepository;
import dataAccess.Student.MyStudentRepository;
import domain.Course;
import domain.CourseType;
import domain.InvalidValueException;
import domain.Student;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Cli {
    Scanner scanner;
    MyCourseRepository courseRepository;
    MyStudentRepository studentRepository;

    public Cli(MyCourseRepository courseRepository, MyStudentRepository studentRepository) {
        this.scanner = new Scanner(System.in);
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }

    public void start() {
        String input = "-";
        while (!input.equals("x")) {
            showMainMenu();
            System.out.print("Eingabe: ");
            input = scanner.nextLine();
            switch (input) {
                case "1" -> startCourse();
                case "2" -> startStudent();
                case "x" -> System.out.println("Auf Wiedersehen!");
                default -> inputError();
            }
        }
        scanner.close();
    }

    private void showMainMenu() {
        printStrich(50, '%');
        System.out.println("\t\t\t\t Mangement-System");
        System.out.println("(1) Kurs-Management \t (2) Studenten-Management");
        System.out.println("\t\t\t\t\t\t (x) ENDE");
        printStrich(50, '%');
    }

    private void printStrich(int i, char zeichen) {
        for (int j = 0; j < i; j++) {
            System.out.print(zeichen);
        }
        System.out.println();
    }

    // Course-Section
    public void startCourse() {
        String input = "-";
        while (!input.equals("x")) {
            showCourseMenu();
            System.out.print("Eingabe: ");
            input = scanner.nextLine();
            switch (input) {
                case "1" -> addCourse();
                case "2" -> showAllCourses();
                case "3" -> showCourseDetails();
                case "4" -> editCourseDetails();
                case "5" -> deleteCourse();
                case "6" -> courseSearch();
                case "7" -> runCourse();
                case "x" -> System.out.println("Auf Wiedersehen!");
                default -> inputError();
            }
        }
    }

    private void runCourse() {
        printStrich(24, '-');
        System.out.println("Aktuell laufende Kurse: ");
        List<Course> courses;
        try {
            courses = courseRepository.findAllRunningCourse();
            for (Course course : courses) {
                System.out.println(course);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Kurs-Anzeige für laufende Kurse: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler beiKurs-Anzeige für laufende Kurse: " + e.getMessage());
        }
    }

    private void courseSearch() {
        printStrich(31, '-');
        System.out.println("Geben Sie einen Suchbegriff an!");
        System.out.print("Eingabe: ");
        String searchText = scanner.nextLine();
        printStrich(31, '-');
        List<Course> coursesList;
        try {
            coursesList = courseRepository.findAllCourseByNameOrDescription(searchText);
            for (Course c : coursesList) {
                System.out.println(c);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbank fehler bei der Kurssuche: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler bei der Kurssuche " + e.getMessage());
        }
    }

    private void deleteCourse() {
        printStrich(33, '-');
        System.out.println("Welchen Kurs möchten Sie löschen?");
        System.out.print("Eingabe: ");
        Long courseIdToDelete = Long.parseLong(scanner.nextLine());
        printStrich(33, '-');
        try {
            courseRepository.deleteById(courseIdToDelete);
        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler beim löschen: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Unbekannter Fehler beim löschen: " + e.getMessage());
            printStrich(91, '!');
        }
    }

    private void editCourseDetails() {
        printStrich(54, '-');
        System.out.println("Für welche Kurs-ID möchten Sie die Kursdetails ändern?");
        System.out.print("Eingabe: ");
        Long courseId = Long.parseLong(scanner.nextLine());
        printStrich(54, '-');
        try {
            Optional<Course> courseOptional = courseRepository.getById(courseId);
            if (courseOptional.isEmpty()) {
                printStrich(50, '$');
                System.out.println("Kurs mit der ID [" + courseId + "] nicht in der Datenbank!");
                printStrich(50, '$');
            } else {
                Course course = courseOptional.get();
                System.out.println("Änderung für folgenden Kurs: ");
                System.out.println(course);
                printStrich(74, '*');
                String name, description, hours, dateFrom, dateTo, courseType;
                System.out.println("Bitte neue Kursdaten angaben (Enter,falls keine Änderungen gewünscht ist): ");

                System.out.print("Name: ");
                name = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Beschreibung: ");
                description = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Stundenanzahl: ");
                hours = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Startdatum (YYYY-MM-DD): ");
                dateFrom = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Enddatum (YYYY-MM-DD): ");
                dateTo = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("KursTyp (ZA/BF/FF/OE): ");
                courseType = scanner.nextLine();
                printStrich(20, '-');

                Optional<Course> optionalCourseUpdated = courseRepository.update(new Course(
                        course.getId(),
                        name.equals("") ? course.getName() : name,
                        description.equals("") ? course.getDescription() : description,
                        hours.equals("") ? course.getHours() : Integer.parseInt(hours),
                        dateFrom.equals("") ? course.getBeginDate() : Date.valueOf(dateFrom),
                        dateTo.equals("") ? course.getEndDate() : Date.valueOf(dateTo),
                        courseType.equals("") ? course.getCourseType() : CourseType.valueOf(courseType)
                ));
                optionalCourseUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Kurs aktualisiert: " + c),
                        () -> System.out.println("Kurs konnte nicht aktualisiert werden!"));
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            printStrich(44, '!');
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
            printStrich(44, '!');
        } catch (InvalidValueException invalidValueException) {
            printStrich(91, '!');
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
            printStrich(91, '!');
        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler bei Kursupdate: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Unbekannter Fehler bei Kursupdate: " + e.getMessage());
            printStrich(91, '!');
        }

    }

    private void addCourse() {
        printStrich(30, '-');
        String name, description;
        int hours;
        Date dateFrom, dateTo;
        CourseType courseType;
        try {
            System.out.println("Bitte alle Kursdaten angeben: ");
            System.out.print("Name: ");
            name = scanner.nextLine();
            if (name.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            printStrich(35, '-');
            System.out.print("Beschreibung: ");
            description = scanner.nextLine();
            if (description.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            printStrich(35, '-');
            System.out.print("Stundenanzahl: ");
            hours = Integer.parseInt(scanner.nextLine());
            printStrich(35, '-');
            System.out.print("Startdatum (YYYY-MM-DD): ");
            dateFrom = Date.valueOf(scanner.nextLine());
            printStrich(35, '-');
            System.out.print("Enddatum (YYYY-MM-DD): ");
            dateTo = Date.valueOf(scanner.nextLine());
            printStrich(35, '-');
            System.out.print("KursTyp (ZA/BF/FF/OE): ");
            courseType = CourseType.valueOf(scanner.nextLine());
            printStrich(35, '-');
            Optional<Course> optionalCourse = courseRepository.insert(new Course(name, description, hours, dateFrom, dateTo, courseType));
            if (optionalCourse.isPresent()) {
                System.out.println("Kurs angelegt: " + optionalCourse.get());
            } else {
                System.out.println("Kurs kann nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            printStrich(44, '!');
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
            printStrich(44, '!');
        } catch (InvalidValueException invalidValueException) {
            printStrich(91, '!');
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
            printStrich(91, '!');
        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler beim einügen: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Unbekannter Fehler beim einfügen: " + e.getMessage());
            printStrich(91, '!');
        }
    }

    private void showCourseDetails() {
        printStrich(55, '-');
        System.out.println("Für welchen Kurs möchten Sie die Kurs Details anzeigen?");
        System.out.print("Eingabe: ");
        Long courseId = Long.parseLong(scanner.nextLine());
        printStrich(55, '-');
        try {
            Optional<Course> courseOptional = courseRepository.getById(courseId);
            if (courseOptional.isPresent()) {
                System.out.println(courseOptional.get());
            } else {
                System.out.println("Kurs mit der ID [" + courseId + "] nicht gefunden!");
            }

        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler bei Kurs-Detailsanzeige: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Ubnbekannter Fehler bei Kurs-Detailsanzeige: " + e.getMessage());
            printStrich(91, '!');
        }
    }

    private void showAllCourses() {
        printStrich(54, '-');
        List<Course> courses = null;
        try {
            courses = courseRepository.getAll();
            if (courses.size() > 0) {
                for (Course course : courses) {
                    System.out.println(course);
                }
            } else {
                System.out.println("Kursliste leer!");
            }
        } catch (DatabaseException databaseException) {
            printStrich(90, '!');
            System.out.println("Datenbankfehler bei Anzeige aller Kurse: " + databaseException.getMessage());
            printStrich(90, '!');
        } catch (Exception e) {
            printStrich(90, '!');
            System.out.println("Unbekannter Fehler bei Anzeige aller Kurse: " + e.getMessage());
            printStrich(90, '!');
        }
    }

    private void inputError() {
        printStrich(50, '#');
        System.out.println("# Bitte nur die Zahlen der Menüauswahl eingeben! #");
        printStrich(50, '#');
    }

    private void showCourseMenu() {
        printStrich(80, '=');
        System.out.println("\t\t\t\t\t\t\t\tKURSMANAGEMENT");
        System.out.println("(1) Kurs anlegen \t\t (2) Alle Kurse anzeigen \t (3) Kursdetails anzeigen ");
        System.out.println("(4) Kursdetails ändern \t (5) Kurs löschen \t\t\t (6) Kurssuche \t");
        System.out.println("(7) Laufende Kurse \t\t (-) ---- \t\t\t (-) ---- \t");
        System.out.println("\t\t\t\t\t\t (x) ENDE");
        printStrich(80, '=');
    }

    // Student-Section
    private void startStudent() {
        String input = "-";
        while (!input.equals("x")) {
            showStudentMenu();
            System.out.print("Eingabe: ");
            input = scanner.nextLine();
            switch (input) {
                case "1" -> addStudent();
                case "2" -> showAllStudent();
                case "3" -> showStudentDetails();
                case "4" -> editStudentDetails();
                case "5" -> deleteStudent();
                case "6" -> studentSearchLike();
                case "7" -> studentSearchVorname();
                case "8" -> studentSearchNachname();
                case "9" -> studentSearchGeburtsdatum();
                case "x" -> System.out.println("Auf Wiedersehen!");
                default -> inputError();
            }
        }
    }

    private void showStudentMenu() {
        printStrich(90, '"');
        System.out.println("\t\t\t\t\t\t\t\tSTUDENT-MANAGEMENT");
        System.out.println("(1) Student anlegen \t\t (2) Alle Student anzeigen \t\t (3) Student-Details anzeigen ");
        System.out.println("(4) Student-Details ändern \t (5) Student löschen \t\t\t (6) Student-Suche (Vor-/ Nachname) \t");
        System.out.println("(7) Student-Suche (Vorname)  (8) Student-Suche (Nachname) \t (9) Student-Suche (Geburtsdatum) \t");
        System.out.println("\t\t\t\t\t\t\t (x) ENDE");
        printStrich(90, '"');
    }

    private void addStudent() {
        printStrich(30, '-');
        String vorname, nachname;
        Date geburtsdatum;
        try {
            System.out.println("Bitte alle Studenten-Daten angeben: ");
            System.out.print("Vorname: ");
            vorname = scanner.nextLine();
            if (vorname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            printStrich(35, '-');
            System.out.print("Nachname: ");
            nachname = scanner.nextLine();
            if (nachname.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            printStrich(35, '-');
            System.out.print("Geburtsdatum (YYYY-MM-DD): ");
            geburtsdatum = Date.valueOf(scanner.nextLine());
            printStrich(35, '-');
            Optional<Student> optionalStudent = studentRepository.insert(new Student(vorname, nachname, geburtsdatum));
            if (optionalStudent.isPresent()) {
                System.out.println("Student angelegt: " + optionalStudent.get());
            } else {
                System.out.println("Student kann nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            printStrich(44, '!');
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
            printStrich(44, '!');
        } catch (InvalidValueException invalidValueException) {
            printStrich(91, '!');
            System.out.println("Studentaen-Daten nicht korrekt angegeben: " + invalidValueException.getMessage());
            printStrich(91, '!');
        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler beim einügen: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Unbekannter Fehler beim einfügen: " + e.getMessage());
            printStrich(91, '!');
        }
    }

    private void showAllStudent() {
        printStrich(54, '-');
        List<Student> students = null;
        try {
            students = studentRepository.getAll();
            if (students.size() > 0) {
                for (Student student : students) {
                    System.out.println(student);
                }
            } else {
                System.out.println("Studenten-Liste leer!");
            }
        } catch (DatabaseException databaseException) {
            printStrich(90, '!');
            System.out.println("Datenbankfehler bei Anzeige aller Studenten: " + databaseException.getMessage());
            printStrich(90, '!');
        } catch (Exception e) {
            printStrich(90, '!');
            System.out.println("Unbekannter Fehler bei Anzeige aller Studenten:  " + e.getMessage());
            printStrich(90, '!');
        }
    }

    private void showStudentDetails() {
        printStrich(55, '-');
        System.out.println("Für welchen Student möchten Sie die Details anzeigen?");
        System.out.print("Eingabe: ");
        Long studentId = Long.parseLong(scanner.nextLine());
        printStrich(55, '-');
        try {
            Optional<Student> studentOptional = studentRepository.getById(studentId);
            if (studentOptional.isPresent()) {
                System.out.println(studentOptional.get());
            } else {
                System.out.println("Student mit der ID [" + studentId + "] nicht gefunden!");
            }

        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler bei Kurs-Detailsanzeige: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Ubnbekannter Fehler bei Kurs-Detailsanzeige: " + e.getMessage());
            printStrich(91, '!');
        }
    }

    private void editStudentDetails() {
        printStrich(54, '-');
        System.out.println("Für welche Studenten-ID möchten Sie die Details ändern?");
        System.out.print("Eingabe: ");
        Long studentId = Long.parseLong(scanner.nextLine());
        printStrich(54, '-');
        try {
            Optional<Student> studentOptional = studentRepository.getById(studentId);
            if (studentOptional.isEmpty()) {
                printStrich(50, '$');
                System.out.println("Student mit der ID [" + studentId + "] nicht in der Datenbank!");
                printStrich(50, '$');
            } else {
                Student student = studentOptional.get();
                System.out.println("Änderung für folgenden Student: ");
                System.out.println(student);
                printStrich(74, '*');
                String vorname, nachname, geburtsdatum;
                System.out.println("Bitte neue Daten angaben (Enter,falls keine Änderungen gewünscht ist): ");

                System.out.print("Vorname: ");
                vorname = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Nachname: ");
                nachname = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Geburtsdatum (YYYY-MM-DD): ");
                geburtsdatum = scanner.nextLine();
                printStrich(20, '-');

                Optional<Student> optionalStudentUpdated = studentRepository.update(new Student(
                        student.getId(),
                        vorname.equals("") ? student.getVorname() : vorname,
                        nachname.equals("") ? student.getNachname() : nachname,
                        geburtsdatum.equals("") ? student.getGeburtsdatum() : Date.valueOf(geburtsdatum)
                ));
                optionalStudentUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Student aktualisiert: " + c),
                        () -> System.out.println("Student konnte nicht aktualisiert werden!"));
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            printStrich(44, '!');
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
            printStrich(44, '!');
        } catch (InvalidValueException invalidValueException) {
            printStrich(91, '!');
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
            printStrich(91, '!');
        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler bei Kursupdate: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Unbekannter Fehler bei Kursupdate: " + e.getMessage());
            printStrich(91, '!');
        }
    }

    private void deleteStudent() {
        printStrich(33, '-');
        System.out.println("Welchen Student möchten Sie löschen?");
        System.out.print("Eingabe: ");
        Long studentIdToDelete = Long.parseLong(scanner.nextLine());
        printStrich(33, '-');
        try {
            studentRepository.deleteById(studentIdToDelete);
        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler beim löschen: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Unbekannter Fehler beim löschen: " + e.getMessage());
            printStrich(91, '!');
        }
    }

    private void studentSearchLike() {
        printStrich(31, '-');
        System.out.println("Geben Sie einen Suchbegriff an!");
        System.out.print("Eingabe: ");
        String searchText = scanner.nextLine();
        printStrich(31, '-');
        List<Student> studentList;
        try {
            studentList = studentRepository.findAllStudentByVornameOrNachname(searchText);
            for (Student student : studentList) {
                System.out.println(student);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbank fehler bei der Studenten-Suche: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler bei der Studenten-Suche " + e.getMessage());
        }
    }

    private void studentSearchVorname() {
        printStrich(31, '-');
        System.out.println("Geben Sie den Vornamen an!");
        System.out.print("Eingabe: ");
        String searchText = scanner.nextLine();
        printStrich(31, '-');
        List<Student> studentList;
        try {
            studentList = studentRepository.findAllStudentByVorname(searchText);
            for (Student student : studentList) {
                System.out.println(student);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbank fehler bei der Studenten-Suche: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler bei der Studenten-Suche " + e.getMessage());
        }
    }

    private void studentSearchNachname() {
        printStrich(31, '-');
        System.out.println("Geben Sie den Nachnamen an!");
        System.out.print("Eingabe: ");
        String searchText = scanner.nextLine();
        printStrich(31, '-');
        List<Student> studentList;
        try {
            studentList = studentRepository.findAllStudentByNachname(searchText);
            for (Student student : studentList) {
                System.out.println(student);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbank fehler bei der Studenten-Suche: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler bei der Studenten-Suche " + e.getMessage());
        }
    }

    private void studentSearchGeburtsdatum() {
        printStrich(31, '-');
        System.out.println("Geben Sie das Geburtsdatum an (YYYY-MM-DD)");
        System.out.print("Eingabe: ");
        try {
            Date searchDate = Date.valueOf(scanner.nextLine());
            printStrich(31, '-');
            List<Student> studentList;
            try {
                studentList = studentRepository.findAllStudentByGeburtsdatum(searchDate);
                for (Student student : studentList) {
                    System.out.println(student);
                }
            } catch (DatabaseException databaseException) {
                System.out.println("Datenbank fehler bei der Studenten-Suche: " + databaseException.getMessage());
            } catch (Exception e) {
                System.out.println("Unbekannter Fehler bei der Studenten-Suche " + e.getMessage());
            }
        } catch(IllegalArgumentException illegalArgumentException){
            printStrich(44, '!');
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
            printStrich(44, '!');
        }
    }
}
