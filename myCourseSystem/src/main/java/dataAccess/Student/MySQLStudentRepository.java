package dataAccess.Student;

import dataAccess.DatabaseException;
import dataAccess.MySQLDatabaseConnection;
import domain.Course;
import domain.CourseType;
import domain.Student;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySQLStudentRepository implements MyStudentRepository {
    private final Connection CON;

    public MySQLStudentRepository() throws SQLException, ClassNotFoundException {
        this.CON = MySQLDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "123");
    }

    @Override
    public Optional<Student> insert(Student entity) {
        Assert.notNull(entity);
        try {
            String sql = "INSERT INTO `student` (`vorname`, `nachname`, `geburtsdatum`) VALUES (?,?,?)";
            PreparedStatement prepareStatement = CON.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            prepareStatement.setString(1, entity.getVorname());
            prepareStatement.setString(2, entity.getNachname());
            prepareStatement.setDate(3, entity.getGeburtsdatum());
            int affectedRows = prepareStatement.executeUpdate();
            if (affectedRows == 0) {
                return Optional.empty();
            }
            ResultSet generatedKeys = prepareStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return this.getById(generatedKeys.getLong(1));
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public Optional<Student> getById(Long id) {
        Assert.notNull(id);
        if (countStudentInDBWithId(id) == 0) {
            return Optional.empty();
        } else {
            try {
                String sql = "SELECT * FROM `student` WHERE `id` = ?";
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setLong(1, id);
                ResultSet resultSet = prepareStatement.executeQuery();
                resultSet.next();
                Student student = new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("vorname"),
                        resultSet.getString("nachname"),
                        resultSet.getDate("geburtsdatum"));
                return Optional.of(student);
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public List<Student> getAll() {
        String sql = "SELECT * FROM `student`";
        try {
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("vorname"),
                        resultSet.getString("nachname"),
                        resultSet.getDate("geburtsdatum"))
                );
            }
            return studentList;
        } catch (SQLException e) {
            throw new DatabaseException("Database error occurred!");
        }
    }

    @Override
    public Optional<Student> update(Student entity) {
        Assert.notNull(entity);
        String sql = "UPDATE `student` SET `vorname` = ?, `nachname` = ?, `geburtsdatum` = ? WHERE `student`.`id` = ?";
        if (countStudentInDBWithId(entity.getId()) == 0) {
            return Optional.empty();
        } else {
            try {
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setString(1, entity.getVorname());
                prepareStatement.setString(2, entity.getNachname());
                prepareStatement.setDate(3, entity.getGeburtsdatum());
                prepareStatement.setLong(4, entity.getId());

                int affectedRows = prepareStatement.executeUpdate();
                if (affectedRows == 0) {
                    return Optional.empty();
                } else {
                    return this.getById(entity.getId());
                }
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);
        String sql = "Delete FROM `student` WHERE `id` = ?";
        try {
            if (countStudentInDBWithId(id) == 1) {
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setLong(1, id);
                prepareStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentByVornameOrNachname(String searchText) {
        try {
            String sql = "SELECT * FROM `student` WHERE LOWER(`vorname`) LIKE LOWER(?) OR LOWER(`nachname`) LIKE LOWER(?)";
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            prepareStatement.setString(1, "%" + searchText + "%");
            prepareStatement.setString(2, "%" + searchText + "%");
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("vorname"),
                        resultSet.getString("nachname"),
                        resultSet.getDate("geburtsdatum")));
            }
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentByVorname(String vorname) {
        try {
            String sql = "SELECT * FROM `student` WHERE LOWER(`vorname`) LIKE LOWER(?) ";
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            prepareStatement.setString(1, "%" + vorname + "%");
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("vorname"),
                        resultSet.getString("nachname"),
                        resultSet.getDate("geburtsdatum")));
            }
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentByNachname(String nachname) {
        try {
            String sql = "SELECT * FROM `student` WHERE LOWER(`nachname`) LIKE LOWER(?)";
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            prepareStatement.setString(1, "%" + nachname + "%");
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("vorname"),
                        resultSet.getString("nachname"),
                        resultSet.getDate("geburtsdatum")));
            }
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentByGeburtsdatum(Date geburtsdatum) {
        try {
            String sql = "SELECT * FROM `student` WHERE `geburtsdatum` = ?";
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            prepareStatement.setDate(1, geburtsdatum);
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("vorname"),
                        resultSet.getString("nachname"),
                        resultSet.getDate("geburtsdatum")));
            }
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    private int countStudentInDBWithId(Long id) {
        try {
            String countSQL = "SELECT COUNT(*) FROM `student` WHERE `id` = ?";
            PreparedStatement prepareStatement = CON.prepareStatement(countSQL);
            prepareStatement.setLong(1, id);
            ResultSet resultSetCount = prepareStatement.executeQuery();
            resultSetCount.next();
            return resultSetCount.getInt(1);
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
}
