package dataAccess.Student;

import dataAccess.BaseRepository;
import domain.Student;

import java.sql.Date;
import java.util.List;

public interface MyStudentRepository extends BaseRepository<Student, Long> {

    List<Student> findAllStudentByVornameOrNachname(String searchText);

    List<Student> findAllStudentByVorname(String vorname);

    List<Student> findAllStudentByNachname(String nachname);

    List<Student> findAllStudentByGeburtsdatum(Date geburtsdatum);
}
