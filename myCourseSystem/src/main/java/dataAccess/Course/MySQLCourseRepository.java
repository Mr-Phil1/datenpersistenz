package dataAccess.Course;

import dataAccess.Course.MyCourseRepository;
import dataAccess.DatabaseException;
import dataAccess.MySQLDatabaseConnection;
import domain.Course;
import domain.CourseType;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySQLCourseRepository implements MyCourseRepository {
    private final Connection CON;

    public MySQLCourseRepository() throws SQLException, ClassNotFoundException {
        this.CON = MySQLDatabaseConnection.getConnection("jdbc:mysql://localhost:3306/kurssystem", "root", "123");
    }

    // CRUD-Methods

    @Override
    public Optional<Course> insert(Course entity) {
        Assert.notNull(entity);
        try {
            String sql = "INSERT INTO `courses` (`name`, `description`, `hours`, `begindate`, `enddate`, `coursetype`) VALUES (?,?,?,?,?,?)";
            PreparedStatement prepareStatement = CON.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            prepareStatement.setString(1, entity.getName());
            prepareStatement.setString(2, entity.getDescription());
            prepareStatement.setInt(3, entity.getHours());
            prepareStatement.setDate(4, entity.getBeginDate());
            prepareStatement.setDate(5, entity.getEndDate());
            prepareStatement.setString(6, entity.getCourseType().toString());
            int affectedRows = prepareStatement.executeUpdate();
            if (affectedRows == 0) {
                return Optional.empty();
            }
            ResultSet generatedKeys = prepareStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return this.getById(generatedKeys.getLong(1));
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public Optional<Course> getById(Long id) {
        Assert.notNull(id);
        if (countCoursesInDBWithId(id) == 0) {
            return Optional.empty();
        } else {
            try {
                String sql = "SELECT * FROM `courses` WHERE `id` = ?";
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setLong(1, id);
                ResultSet resultSet = prepareStatement.executeQuery();
                resultSet.next();
                Course course = new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("endDate"),
                        CourseType.valueOf(resultSet.getString("coursetype")));
                return Optional.of(course);
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }

    }

    @Override
    public List<Course> getAll() {
        String sql = "SELECT * FROM `courses`";
        try {
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Course> coursesList = new ArrayList<>();
            while (resultSet.next()) {
                coursesList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("endDate"),
                        CourseType.valueOf(resultSet.getString("coursetype")))
                );
            }
            return coursesList;
        } catch (SQLException e) {
            throw new DatabaseException("Database error occurred!");
        }
    }

    @Override
    public Optional<Course> update(Course entity) {
        Assert.notNull(entity);
        String sql = "UPDATE `courses` SET `name` = ?, `description` = ?, `hours` = ?, `begindate` = ?, `enddate` = ?, `coursetype` = ? WHERE `courses`.`id` = ?";
        if (countCoursesInDBWithId(entity.getId()) == 0) {
            return Optional.empty();
        } else {
            try {
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setString(1, entity.getName());
                prepareStatement.setString(2, entity.getDescription());
                prepareStatement.setInt(3, entity.getHours());
                prepareStatement.setDate(4, entity.getBeginDate());
                prepareStatement.setDate(5, entity.getEndDate());
                prepareStatement.setString(6, entity.getCourseType().toString());
                prepareStatement.setLong(7, entity.getId());

                int affectedRows = prepareStatement.executeUpdate();
                if (affectedRows == 0) {
                    return Optional.empty();
                } else {
                    return this.getById(entity.getId());
                }
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }

    @Override
    public void deleteById(Long id) {
        Assert.notNull(id);
        String sql = "Delete FROM `courses` WHERE `id` = ?";
        try {
            if (countCoursesInDBWithId(id) == 1) {
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setLong(1, id);
                prepareStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }
    }

    @Override
    public List<Course> findAllCourseByName(String name) {
        return null;
    }

    @Override
    public List<Course> findAllCourseByDescription(String description) {
        return null;
    }

    @Override
    public List<Course> findAllCourseByNameOrDescription(String searchText) {
        try {
            String sql = "SELECT * FROM `courses` WHERE LOWER(`description`) LIKE LOWER(?) OR LOWER(`name`) LIKE LOWER(?)";
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            prepareStatement.setString(1, "%" + searchText + "%");
            prepareStatement.setString(2, "%" + searchText + "%");
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Course> coursesList = new ArrayList<>();
            while (resultSet.next()) {
                coursesList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("endDate"),
                        CourseType.valueOf(resultSet.getString("coursetype"))));
            }
            return coursesList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    @Override
    public List<Course> findAllCourseByStartDate(Date startDate) {
        return null;
    }

    @Override
    public List<Course> findAllCourseByCourseType(CourseType courseType) {
        return null;
    }

    @Override
    public List<Course> findAllRunningCourse() {
        String sql = "SELECT * FROM `courses` WHERE NOW() < `endDate`";
        try {
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Course> coursesList = new ArrayList<>();
            while (resultSet.next()) {
                coursesList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("endDate"),
                        CourseType.valueOf(resultSet.getString("coursetype"))));


            }
            return coursesList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }

    private int countCoursesInDBWithId(Long id) {
        try {
            String countSQL = "SELECT COUNT(*) FROM `courses` WHERE `id` = ?";
            PreparedStatement prepareStatement = CON.prepareStatement(countSQL);
            prepareStatement.setLong(1, id);
            ResultSet resultSetCount = prepareStatement.executeQuery();
            resultSetCount.next();
            return resultSetCount.getInt(1);
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
}
