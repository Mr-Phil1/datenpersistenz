import java.sql.*;

public class JdbcDemo {
    public static void main(String[] args) {
        System.out.println("JDBC Demo");
        selectAllDemo();
        //  insertStudentDemo("Name des Studenten", "email@prov.at");
        selectAllDemo();
        updateStudentDemo(6, "Neuer Name", "neuer.name@provider.at");
        selectAllDemo();
        deleteStudentDemo(5);
        selectAllDemo();
        findAllByNameLike("land");
    }




    public static void deleteStudentDemo(int studentId) {
        System.out.println("Delete Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "DELETE FROM `student` WHERE `student`.`id` = ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            try {
                prepareStatement.setInt(1, studentId);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println("Anzahl der gelöschten Datensätze: " + rowAffected);
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Delete Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }

    public static void updateStudentDemo(int studentId, String neuerName, String neueEmail) {
        System.out.println("Update Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "UPDATE `student` SET `name` = ?, `email` = ? WHERE `student`.`id` = ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            try {
                prepareStatement.setString(1, neuerName);
                prepareStatement.setString(2, neueEmail);
                prepareStatement.setInt(3, studentId);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println("Anzahl der aktualisierten Datensätze: " + rowAffected);
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Update Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }

    private static void findAllByNameLike(String pattern) {

        System.out.println("FindAllByNameLike Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "Select * FROM `student` WHERE `student`.`name` LIKE  ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            prepareStatement.setString(1, "%" + pattern + "%");
            ResultSet rs = prepareStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: [id] " + id + ", [name] " + name + ", [email] " + email);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }

    public static void insertStudentDemo(String name, String email) {
        System.out.println("Insert Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "INSERT INTO `student` (`id`, `name`, `email`) VALUES (NULL, ?, ?)";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            try {
                prepareStatement.setString(1, name);
                prepareStatement.setString(2, email);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze eingefügt");
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Insert Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }

    public static void selectAllDemo() {
        System.out.println("Select Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "SELECT * FROM `student`";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            ResultSet rs = prepareStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: [id] " + id + ", [name] " + name + ", [email] " + email);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }


}
