import java.sql.*;

public class JdbcDemoAdresse {
    public static void main(String[] args) {
        selectAllDemo();
        insertAdresseDemo("Salzburg", 5020);
        selectAllDemo();
        updateAdresseDemo(2, "Lienz", 9900);
        selectAllDemo();
        findAllByPlzLike(60);
        deleteAdresseDemo(6);
        selectAllDemo();
    }


    public static void selectAllDemo() {
        System.out.println();
        System.out.println("Select Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllAdress = "SELECT * FROM `adresse`";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllAdress);
            ResultSet rs = prepareStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String ort = rs.getString("ort");
                int plz = rs.getInt("plz");
                System.out.println("Student aus der DB: [id] " + id + ", [plz] " + plz + ", [ort] " + ort);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }

    /**
     * @param ort
     * @param plz
     */
    public static void insertAdresseDemo(String ort, int plz) {
        System.out.println();
        System.out.println("Insert Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlInsertAdresse = "INSERT INTO `adresse` (`id`, `ort`, `plz`) VALUES (NULL, ?, ?)";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlInsertAdresse);
            try {
                prepareStatement.setString(1, ort);
                prepareStatement.setInt(2, plz);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze eingefügt");
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Insert Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }

    /**
     * @param adresseId
     * @param neuerOrtsName
     * @param neuePlz
     */
    public static void updateAdresseDemo(int adresseId, String neuerOrtsName, int neuePlz) {
        System.out.println();
        System.out.println("Update Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlUpdateAdresse = "UPDATE `adresse` SET `ort` = ?, `plz` = ? WHERE `adresse`.`id` = ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlUpdateAdresse);
            try {
                prepareStatement.setString(1, neuerOrtsName);
                prepareStatement.setInt(2, neuePlz);
                prepareStatement.setInt(3, adresseId);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println("Anzahl der aktualisierten Datensätze: " + rowAffected);
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Update Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }

    /**
     * @param pattern
     */
    public static void findAllByPlzLike(int pattern) {
        System.out.println();
        System.out.println("FindAllByNameLike Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "Select * FROM `adresse` WHERE `adresse`.`plz` LIKE  ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            prepareStatement.setString(1, "%" + pattern + "%");
            ResultSet rs = prepareStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String ort = rs.getString("ort");
                int plz = rs.getInt("plz");
                System.out.println("Student aus der DB: [id] " + id + ", [plz] " + plz + ", [ort] " + ort);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }

    /**
     * @param studentId
     */
    public static void deleteAdresseDemo(int studentId) {
        System.out.println();
        System.out.println("Delete Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlDelete = "DELETE FROM `adresse` WHERE `adresse`.`id` = ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlDelete);
            try {
                prepareStatement.setInt(1, studentId);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println("Anzahl der gelöschten Datensätze: " + rowAffected);
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Delete Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }

}
