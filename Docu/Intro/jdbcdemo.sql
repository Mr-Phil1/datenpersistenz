-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Generation Time: Mar 06, 2022 at 07:05 AM
-- Server version: 8.0.28
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jdbcdemo`
--
CREATE DATABASE IF NOT EXISTS `jdbcdemo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `jdbcdemo`;

-- --------------------------------------------------------

--
-- Table structure for table `adresse`
--

CREATE TABLE `adresse` (
  `id` int NOT NULL,
  `ort` varchar(200) NOT NULL,
  `plz` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `adresse`
--

INSERT INTO `adresse` (`id`, `ort`, `plz`) VALUES
(1, 'Innsbruck', 6020),
(2, 'Lienz', 9900),
(3, 'Hall in Tirol', 6060),
(4, 'Imst', 6460),
(5, 'Lienz', 9900),
(7, 'Wien', 1010),
(8, 'Salzburg', 5020),
(9, 'Salzburg', 5020);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `email`) VALUES
(1, 'Claudio Landerer', 'claudio.landerer@myimst.at'),
(2, 'Maria Könne', 'maria.koenne@myimst.at'),
(3, 'Helmut Zangerl', 'h.zangerl@mynetwork.com'),
(4, 'Hans Zimmer', 'h.zi765@trx.at'),
(6, 'Neuer Name', 'neuer.name@provider.at'),
(7, 'Peter Zeck', 'p.zeck@hotmail.com'),
(8, 'Peter Zeck', 'p.zeck@hotmail.com'),
(9, 'Name des Studenten', 'email@prov.at'),
(10, 'Name des Studenten', 'email@prov.at'),
(11, 'Name des Studenten', 'email@prov.at'),
(12, 'Name des Studenten', 'email@prov.at'),
(13, 'Name des Studenten', 'email@prov.at');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
