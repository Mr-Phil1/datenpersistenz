---
title: Übungszettel
subtitle: Datenpersistenz
author: Mr. Phil
rights: Nah
language: de-AT
keywords: POS; LAND;
titlepage: true
titlepage-color: "BFBFBF"
titlepage-text-color: "000000"
table-use-row-colors: true
toc-own-page: true
---

# Maven Projekt

## Erstellung

![IntelliJ Maven Archetype Auswahl](Bilder/03_IntelliJ-Maven_01.png) {height=500px width=500px}

![IntelliJ Maven Namen und Speicherort Auswahl](Bilder/03_IntelliJ-Maven_02.png) {height=500px width=500px}

\pagebreak

## MySQL-Connector-Java Dependency

```xml
<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.28</version>
</dependency>
```

# Datenbank

## Aufgabe 1

### Tabellenaufbau

```sql
CREATE TABLE IF NOT EXISTS `student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

### Daten einfügen

```sql
INSERT INTO `student` (`id`, `name`, `email`) VALUES (NULL, 'Claudio Landerer', 'claudio.landerer@myimst.at'), (NULL, 'Maria Könne', 'maria.koenne@myimst.at');
```

![Select all](Bilder/01_Select-all.png)

### Daten ausgeben mit Like

```sql
SELECT * FROM `student` WHERE `name` LIKE '%landerer%';
```

![Select with LIKE](Bilder/02_Select-with-like.png)

![InteliJ MySQL Tabelle Student](Bilder/05_Inhalt-student-tabelle.png)

\pagebreak

## Aufgabe 2

### Tabellenaufbau

```sql
CREATE TABLE `adresse` (
  `id` int NOT NULL,
  `ort` varchar(200) NOT NULL,
  `plz` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```

![InteliJ MySQL Tabelle Adresse](Bilder/06_Inhalt-adresse-tabelle.png)

\pagebreak

# Java - Aufgabe 1

## selectAllDemo

### Code

```java
public static void selectAllDemo() {
        System.out.println("Select Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "SELECT * FROM `student`";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            ResultSet rs = prepareStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: [id] "+id+", [name] "+name+", [email] "+email);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

### Ausgabe

```text
Select Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [name] Claudio Landerer, [email] claudio.landerer@myimst.at
Student aus der DB: [id] 2, [name] Maria Könne, [email] maria.koenne@myimst.at
Student aus der DB: [id] 3, [name] Helmut Zangerl, [email] h.zangerl@mynetwork.com
```

\pagebreak

## insertStudentDemo

### Code

```java
 public static void insertStudentDemo() {
        System.out.println("Insert Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "INSERT INTO `student` (`id`, `name`, `email`) VALUES (NULL, ?, ?)";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            try {
                prepareStatement.setString(1, "Peter Zeck");
                prepareStatement.setString(2, "p.zeck@hotmail.com");
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze eingefügt");
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Insert Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

### Ausgabe

```text
Insert Demo mit JDBC
Verbindung zur Datenbank hergestellt
1 Datensätze eingefügt

Select Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [name] Claudio Landerer, [email] claudio.landerer@myimst.at
Student aus der DB: [id] 2, [name] Maria Könne, [email] maria.koenne@myimst.at
Student aus der DB: [id] 3, [name] Helmut Zangerl, [email] h.zangerl@mynetwork.com
Student aus der DB: [id] 4, [name] Peter Zeck, [email] p.zeck@hotmail.com
```

\pagebreak

## updateStudentDemo

### Code

```java
public static void updateStudentDemo() {
        System.out.println("Update Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "UPDATE `student` SET `name` = ?, `email` = ? WHERE `student`.`id` = 4";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            try {
                prepareStatement.setString(1, "Hans Zimmer");
                prepareStatement.setString(2, "h.zi765@trx.at");
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println("Anzahl der aktualisierten Datensätze: " + rowAffected);
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Update Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

\pagebreak

### Ausgabe

```text
Update Demo mit JDBC
Verbindung zur Datenbank hergestellt
Anzahl der aktualisierten Datensätze: 1

Select Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [name] Claudio Landerer, [email] claudio.landerer@myimst.at
Student aus der DB: [id] 2, [name] Maria Könne, [email] maria.koenne@myimst.at
Student aus der DB: [id] 3, [name] Helmut Zangerl, [email] h.zangerl@mynetwork.com
Student aus der DB: [id] 4, [name] Hans Zimmer, [email] h.zi765@trx.at
Student aus der DB: [id] 5, [name] Peter Zeck, [email] p.zeck@hotmail.com
```

\pagebreak

## deleteStudentDemo

### Code

```java
 public static void deleteStudentDemo(int studentId) {
        System.out.println("Delete Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "DELETE FROM `student` WHERE `student`.`id` = ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            try {
                prepareStatement.setInt(1, studentId);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println("Anzahl der gelöschten Datensätze: " + rowAffected);
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Delete Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

\pagebreak

### Ausgabe

```text
Delete Demo mit JDBC
Verbindung zur Datenbank hergestellt
Anzahl der gelöschten Datensätze: 1

Select Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [name] Claudio Landerer, [email] claudio.landerer@myimst.at
Student aus der DB: [id] 2, [name] Maria Könne, [email] maria.koenne@myimst.at
Student aus der DB: [id] 3, [name] Helmut Zangerl, [email] h.zangerl@mynetwork.com
Student aus der DB: [id] 4, [name] Hans Zimmer, [email] h.zi765@trx.at
Student aus der DB: [id] 6, [name] Peter Zeck, [email] p.zeck@hotmail.com
Student aus der DB: [id] 7, [name] Peter Zeck, [email] p.zeck@hotmail.com
```

\pagebreak

## findAllByNameLike

### code

```java
private static void findAllByNameLike(String pattern) {
        System.out.println("FindAllByNameLike Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "Select * FROM `student` WHERE `student`.`name` LIKE  ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            prepareStatement.setString(1, "%"+pattern+"%");
            ResultSet rs = prepareStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println("Student aus der DB: [id] " + id + ", [name] " + name + ", [email] " + email);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

### Ausgabe

```text
FindAllByNameLike Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [name] Claudio Landerer, [email] claudio.landerer@myimst.at
```

\pagebreak

# Java - Aufgabe 2

## selectAllDemo

### code

```java
public static void selectAllDemo() {
        System.out.println();
        System.out.println("Select Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllAdress = "SELECT * FROM `adresse`";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllAdress);
            ResultSet rs = prepareStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String ort = rs.getString("ort");
                int plz = rs.getInt("plz");
                System.out.println("Student aus der DB: [id] " + id + ", [plz] " + plz + ", [ort] " + ort);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

### Ausgabe

```text
Select Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [plz] 6020, [ort] Innsbruck
```

\pagebreak

## insertAdresseDemo

### Code

```java
public static void insertAdresseDemo(String ort, int plz){
        System.out.println();
        System.out.println("Insert Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlInsertAdresse = "INSERT INTO `adresse` (`id`, `ort`, `plz`) VALUES (NULL, ?, ?)";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlInsertAdresse);
            try {
                prepareStatement.setString(1, ort);
                prepareStatement.setInt(2, plz);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze eingefügt");
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Insert Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

### Ausgabe

```text
Insert Demo mit JDBC
Verbindung zur Datenbank hergestellt
1 Datensätze eingefügt

Select Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [plz] 6020, [ort] Innsbruck
Student aus der DB: [id] 2, [plz] 6020, [ort] Innsbruck
```

\pagebreak

## updateAdresseDemo

### Code

```java
public static void  updateAdresseDemo(int adresseId, String neuerOrtsName, int neuePlz){
        System.out.println();
        System.out.println("Update Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlUpdateAdresse = "UPDATE `adresse` SET `ort` = ?, `plz` = ? WHERE `adresse`.`id` = ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlUpdateAdresse);
            try {
                prepareStatement.setString(1, neuerOrtsName);
                prepareStatement.setInt(2, neuePlz);
                prepareStatement.setInt(3, adresseId);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println("Anzahl der aktualisierten Datensätze: " + rowAffected);
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Update Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

### Ausgabe

```text
Update Demo mit JDBC
Verbindung zur Datenbank hergestellt
Anzahl der aktualisierten Datensätze: 1

Select Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [plz] 6020, [ort] Innsbruck
Student aus der DB: [id] 2, [plz] 6020, [ort] Innsbruck
Student aus der DB: [id] 3, [plz] 6060, [ort] Hall in Tirol
```

\pagebreak

## findAllByPlzLike

### Code

```java
 public static void findAllByPlzLike(int pattern) {
        System.out.println();
        System.out.println("FindAllByNameLike Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlSelectAllPerson = "Select * FROM `adresse` WHERE `adresse`.`plz` LIKE  ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlSelectAllPerson);
            prepareStatement.setString(1, "%" + pattern + "%");
            ResultSet rs = prepareStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String ort = rs.getString("ort");
                int plz = rs.getInt("plz");
                System.out.println("Student aus der DB: [id] " + id + ", [plz] " + plz + ", [ort] " + ort);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

\pagebreak

### Ausgabe

```text
Select Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [plz] 6020, [ort] Innsbruck
Student aus der DB: [id] 2, [plz] 9900, [ort] Lienz
Student aus der DB: [id] 3, [plz] 6060, [ort] Hall in Tirol
Student aus der DB: [id] 4, [plz] 6460, [ort] Imst
Student aus der DB: [id] 5, [plz] 9900, [ort] Lienz
Student aus der DB: [id] 6, [plz] 1010, [ort] Wien
Student aus der DB: [id] 7, [plz] 1010, [ort] Wien
Student aus der DB: [id] 8, [plz] 5020, [ort] Salzburg

FindAllByNameLike Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [plz] 6020, [ort] Innsbruck
Student aus der DB: [id] 3, [plz] 6060, [ort] Hall in Tirol
Student aus der DB: [id] 4, [plz] 6460, [ort] Imst
```

\pagebreak

## deleteAdresseDemo

### Code

```java
public static void deleteAdresseDemo(int studentId) {
        System.out.println();
        System.out.println("Delete Demo mit JDBC");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String username = "root";
        String password = "123";
        try (Connection conn = DriverManager.getConnection(connectionUrl, username, password)) {
            System.out.println("Verbindung zur Datenbank hergestellt");
            String sqlDelete = "DELETE FROM `adresse` WHERE `adresse`.`id` = ?";
            PreparedStatement prepareStatement = conn.prepareStatement(sqlDelete);
            try {
                prepareStatement.setInt(1, studentId);
                int rowAffected = prepareStatement.executeUpdate();
                System.out.println("Anzahl der gelöschten Datensätze: " + rowAffected);
            } catch (SQLException ex) {
                System.out.println("Fehler im SQL-Delete Statement : " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verindung zur Datenbank: " + e.getMessage());
        }
    }
```

\pagebreak

### Ausgabe

```text
Delete Demo mit JDBC
Verbindung zur Datenbank hergestellt
Anzahl der gelöschten Datensätze: 1

Select Demo mit JDBC
Verbindung zur Datenbank hergestellt
Student aus der DB: [id] 1, [plz] 6020, [ort] Innsbruck
Student aus der DB: [id] 2, [plz] 9900, [ort] Lienz
Student aus der DB: [id] 3, [plz] 6060, [ort] Hall in Tirol
Student aus der DB: [id] 4, [plz] 6460, [ort] Imst
Student aus der DB: [id] 5, [plz] 9900, [ort] Lienz
Student aus der DB: [id] 7, [plz] 1010, [ort] Wien
Student aus der DB: [id] 8, [plz] 5020, [ort] Salzburg
Student aus der DB: [id] 9, [plz] 5020, [ort] Salzburg
```

\pagebreak

# Literatur- / Abbildungsverzeichnis

## Literaturverzeichnis

- [Maven Repository](https://mvnrepository.com/)
  - [MySQL-Connector-Java](https://mvnrepository.com/artifact/mysql/mysql-connector-java)

## Abbildungsvezeichnis

\renewcommand{\listfigurename}{}
\listoffigures
