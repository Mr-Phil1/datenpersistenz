---
title: Übungszettel
subtitle: Datenpersistenz
author: Mr. Phil
rights: Nah
language: de-AT
keywords: POS; LAND;
titlepage: true
titlepage-color: "BFBFBF"
titlepage-text-color: "000000"
table-use-row-colors: true
toc-own-page: true
---

# Maven Project

## MySQL-Connector-Java Dependency

```xml
<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.28</version>
</dependency>
```

\pagebreak

# Datenbankaufbau

## MySQLDatabaseConnection

Diese Klasse ist für die Verbindung JAVA-Program <-> Datenbank zuständig.
Hier bei muss man beim Aufruf der Methode `MySQLDatabaseConnection.getConnection` die URL, denn User und dessen Passwort mitgeben. (z.B.: `jdbc:mysql://localhost:3306/kurssystem", "root", "123"`)

```java
public class MySQLDatabaseConnection {
    private static Connection con = null;

    private MySQLDatabaseConnection() {

    }

    public static Connection getConnection(String url, String username, String password) throws ClassNotFoundException, SQLException {
        if (con != null) {
            return con;
        } else {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, username, password);
            return con;
        }
    }
}
```

\pagebreak

# Kurse

## CRUD-Methoden

### insert

```java
public Optional<Course> insert(Course entity) {
        Assert.notNull(entity);
        try {
            String sql = "INSERT INTO `courses` (`name`, `description`, `hours`, `begindate`, `enddate`, `coursetype`) VALUES (?,?,?,?,?,?)";
            PreparedStatement prepareStatement = CON.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            prepareStatement.setString(1, entity.getName());
            prepareStatement.setString(2, entity.getDescription());
            prepareStatement.setInt(3, entity.getHours());
            prepareStatement.setDate(4, entity.getBeginDate());
            prepareStatement.setDate(5, entity.getEndDate());
            prepareStatement.setString(6, entity.getCourseType().toString());
            int affectedRows = prepareStatement.executeUpdate();
            if (affectedRows == 0) {
                return Optional.empty();
            }
            ResultSet generatedKeys = prepareStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return this.getById(generatedKeys.getLong(1));
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
```

\pagebreak

### update

```java
public Optional<Course> update(Course entity) {
        Assert.notNull(entity);
        String sql = "UPDATE `courses` SET `name` = ?, `description` = ?, `hours` = ?, `begindate` = ?, `enddate` = ?, `coursetype` = ? WHERE `courses`.`id` = ?";
        if (countCoursesInDBWithId(entity.getId()) == 0) {
            return Optional.empty();
        } else {
            try {
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setString(1, entity.getName());
                prepareStatement.setString(2, entity.getDescription());
                prepareStatement.setInt(3, entity.getHours());
                prepareStatement.setDate(4, entity.getBeginDate());
                prepareStatement.setDate(5, entity.getEndDate());
                prepareStatement.setString(6, entity.getCourseType().toString());
                prepareStatement.setLong(7, entity.getId());

                int affectedRows = prepareStatement.executeUpdate();
                if (affectedRows == 0) {
                    return Optional.empty();
                } else {
                    return this.getById(entity.getId());
                }
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }
```

\pagebreak

### deleteById

```java
 public void deleteById(Long id) {
        Assert.notNull(id);
        String sql = "Delete FROM `courses` WHERE `id` = ?";
        try {
            if (countCoursesInDBWithId(id) == 1) {
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setLong(1, id);
                prepareStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }
    }
```

\pagebreak

### getAll

```java
 public List<Course> getAll() {
        String sql = "SELECT * FROM `courses`";
        try {
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Course> coursesList = new ArrayList<>();
            while (resultSet.next()) {
                coursesList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("endDate"),
                        CourseType.valueOf(resultSet.getString("coursetype")))
                );
            }
            return coursesList;
        } catch (SQLException e) {
            throw new DatabaseException("Database error occurred!");
        }
    }
```

\pagebreak

### getById

```java
public Optional<Course> getById(Long id) {
        Assert.notNull(id);
        if (countCoursesInDBWithId(id) == 0) {
            return Optional.empty();
        } else {
            try {
                String sql = "SELECT * FROM `courses` WHERE `id` = ?";
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setLong(1, id);
                ResultSet resultSet = prepareStatement.executeQuery();
                resultSet.next();
                Course course = new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("endDate"),
                        CourseType.valueOf(resultSet.getString("coursetype")));
                return Optional.of(course);
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }
```

\pagebreak

## spezifische-Methoden

### findAllCourseByNameOrDescription

```java
public List<Course> findAllCourseByNameOrDescription(String searchText) {
        try {
            String sql = "SELECT * FROM `courses` WHERE LOWER(`description`) LIKE LOWER(?) OR LOWER(`name`) LIKE LOWER(?)";
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            prepareStatement.setString(1, "%" + searchText + "%");
            prepareStatement.setString(2, "%" + searchText + "%");
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Course> coursesList = new ArrayList<>();
            while (resultSet.next()) {
                coursesList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("endDate"),
                        CourseType.valueOf(resultSet.getString("coursetype"))));
            }
            return coursesList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
```

\pagebreak

### findAllRunningCourse

```java
    public List<Course> findAllRunningCourse() {
        String sql = "SELECT * FROM `courses` WHERE NOW() < `endDate`";
        try {
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Course> coursesList = new ArrayList<>();
            while (resultSet.next()) {
                coursesList.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("endDate"),
                        CourseType.valueOf(resultSet.getString("coursetype"))));


            }
            return coursesList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
```

\pagebreak

### findAllCourseByName

```java

```

\pagebreak

### findAllCourseByDescription

```java

```

\pagebreak

### findAllCourseByStartDate

```java

```

\pagebreak

### findAllCourseByCourseType

```java

```

\pagebreak

# Studenten

## CRUD-Methoden

### insert

```java
 public Optional<Student> insert(Student entity) {
        Assert.notNull(entity);
        try {
            String sql = "INSERT INTO `student` (`vorname`, `nachname`, `geburtsdatum`) VALUES (?,?,?)";
            PreparedStatement prepareStatement = CON.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            prepareStatement.setString(1, entity.getVorname());
            prepareStatement.setString(2, entity.getNachname());
            prepareStatement.setDate(3, entity.getGeburtsdatum());
            int affectedRows = prepareStatement.executeUpdate();
            if (affectedRows == 0) {
                return Optional.empty();
            }
            ResultSet generatedKeys = prepareStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return this.getById(generatedKeys.getLong(1));
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
```

\pagebreak

### update

```java
public Optional<Student> update(Student entity) {
        Assert.notNull(entity);
        String sql = "UPDATE `student` SET `vorname` = ?, `nachname` = ?, `geburtsdatum` = ? WHERE `student`.`id` = ?";
        if (countStudentInDBWithId(entity.getId()) == 0) {
            return Optional.empty();
        } else {
            try {
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setString(1, entity.getVorname());
                prepareStatement.setString(2, entity.getNachname());
                prepareStatement.setDate(3, entity.getGeburtsdatum());
                prepareStatement.setLong(4, entity.getId());

                int affectedRows = prepareStatement.executeUpdate();
                if (affectedRows == 0) {
                    return Optional.empty();
                } else {
                    return this.getById(entity.getId());
                }
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }
```

\pagebreak

### deleteById

```java
public void deleteById(Long id) {
        Assert.notNull(id);
        String sql = "Delete FROM `student` WHERE `id` = ?";
        try {
            if (countStudentInDBWithId(id) == 1) {
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setLong(1, id);
                prepareStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }
    }
```

\pagebreak

### getAll

```java
public List<Student> getAll() {
        String sql = "SELECT * FROM `student`";
        try {
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("vorname"),
                        resultSet.getString("nachname"),
                        resultSet.getDate("geburtsdatum"))
                );
            }
            return studentList;
        } catch (SQLException e) {
            throw new DatabaseException("Database error occurred!");
        }
    }
```

\pagebreak

### getById

```java
 public Optional<Student> getById(Long id) {
        Assert.notNull(id);
        if (countStudentInDBWithId(id) == 0) {
            return Optional.empty();
        } else {
            try {
                String sql = "SELECT * FROM `student` WHERE `id` = ?";
                PreparedStatement prepareStatement = CON.prepareStatement(sql);
                prepareStatement.setLong(1, id);
                ResultSet resultSet = prepareStatement.executeQuery();
                resultSet.next();
                Student student = new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("vorname"),
                        resultSet.getString("nachname"),
                        resultSet.getDate("geburtsdatum"));
                return Optional.of(student);
            } catch (SQLException sqlException) {
                throw new DatabaseException(sqlException.getMessage());
            }
        }
    }
```

\pagebreak

## spezifische-Methoden

### findAllStudentByVornameOrNachname

```java
public List<Student> findAllStudentByVornameOrNachname(String searchText) {
        try {
            String sql = "SELECT * FROM `student` WHERE LOWER(`vorname`) LIKE LOWER(?) OR LOWER(`nachname`) LIKE LOWER(?)";
            PreparedStatement prepareStatement = CON.prepareStatement(sql);
            prepareStatement.setString(1, "%" + searchText + "%");
            prepareStatement.setString(2, "%" + searchText + "%");
            ResultSet resultSet = prepareStatement.executeQuery();
            ArrayList<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("vorname"),
                        resultSet.getString("nachname"),
                        resultSet.getDate("geburtsdatum")));
            }
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
```

\pagebreak

### findAllStudentByVorname

```java

```

\pagebreak

### findAllStudentByNachname

```java

```

\pagebreak

### findAllStudentByGeburtsdatum

```java

```

\pagebreak

### countStudentInDBWithId

```java
    private int countStudentInDBWithId(Long id) {
        try {
            String countSQL = "SELECT COUNT(*) FROM `student` WHERE `id` = ?";
            PreparedStatement prepareStatement = CON.prepareStatement(countSQL);
            prepareStatement.setLong(1, id);
            ResultSet resultSetCount = prepareStatement.executeQuery();
            resultSetCount.next();
            return resultSetCount.getInt(1);
        } catch (SQLException sqlException) {
            throw new DatabaseException(sqlException.getMessage());
        }
    }
```

# Exception

## InvalidValueException

Diese Exception wird geworfen wenn die eingeben Daten nicht zum Umwandeln in andere Datentypen Kompatibel sind. (z.B.: double -> int)

```java
public class InvalidValueException extends RuntimeException {
    public InvalidValueException(String message) {
        super(message);
    }
}
```

## DatabaseException

Diese Exception wird geworfen wenn beim Verbindungsaufbau/ beim Ausführen von Datenbank-Befehlen irgenwas nicht 100% richtig läuft.

```java
public class DatabaseException extends RuntimeException {
    public DatabaseException(String message) {
        super(message);
    }
}
```

\pagebreak

# CLI

Die folgenden Methoden sind für die Ausführung als Commandline-Programm zusändig.

## Haupt

### Constructor

```java
    Scanner scanner;
    MyCourseRepository courseRepository;
    MyStudentRepository studentRepository;

    public Cli(MyCourseRepository courseRepository, MyStudentRepository studentRepository) {
        this.scanner = new Scanner(System.in);
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }
```

### start

Dies ist die Steuerung für das allgemeine Startmenü.

```java
    public void start() {
        String input = "-";
        while (!input.equals("x")) {
            showMainMenu();
            System.out.print("Eingabe: ");
            input = scanner.nextLine();
            switch (input) {
                case "1" -> startCourse();
                case "2" -> startStudent();
                case "x" -> System.out.println("Auf Wiedersehen!");
                default -> inputError();
            }
        }
        scanner.close();
    }
```

### showMainMenu

Dies ist der Text für das allgemeine Startmenü.

```java
 private void showMainMenu() {
        printStrich(50, '%');
        System.out.println("\t\t\t\t Mangement-System");
        System.out.println("(1) Kurs-Management \t (2) Studenten-Management");
        System.out.println("\t\t\t\t\t\t (x) ENDE");
        printStrich(50, '%');
    }
```

### inputError

Sollte man eine falsche Eingabe gemacht haben, so wird diese Methode ausgeführt.

```java
  private void inputError() {
        printStrich(50, '#');
        System.out.println("# Bitte nur die Zahlen der Menüauswahl eingeben! #");
        printStrich(50, '#');
    }
```

### printStrich

Diese Methode ist für das Abtrennen von den verschieden Bereichen zuständig. Als Parametern muss man einfach nur die Länge und das Sympol mitgegeben. Danach bekommt man einen Strich mit der geünschten Länge und Sympol.

```java
  private void printStrich(int i, char zeichen) {
        for (int j = 0; j < i; j++) {
            System.out.print(zeichen);
        }
        System.out.println();
    }
```

\pagebreak

## Kurse

### startCourse

Dies ist die Menüsteuerung für das KURSMANAGEMENT.

```java
public void startCourse() {
        String input = "-";
        while (!input.equals("x")) {
            showCourseMenu();
            System.out.print("Eingabe: ");
            input = scanner.nextLine();
            switch (input) {
                case "1" -> addCourse();
                case "2" -> showAllCourses();
                case "3" -> showCourseDetails();
                case "4" -> editCourseDetails();
                case "5" -> deleteCourse();
                case "6" -> courseSearch();
                case "7" -> runCourse();
                case "x" -> System.out.println("Auf Wiedersehen!");
                default -> inputError();
            }
        }
    }
```

\pagebreak

### runCourse

```java
private void runCourse() {
        printStrich(24, '-');
        System.out.println("Aktuell laufende Kurse: ");
        List<Course> courses;
        try {
            courses = courseRepository.findAllRunningCourse();
            for (Course course : courses) {
                System.out.println(course);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler bei Kurs-Anzeige für laufende Kurse: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler beiKurs-Anzeige für laufende Kurse: " + e.getMessage());
        }
    }
```

\pagebreak

### courseSearch

```java
 private void courseSearch() {
        printStrich(31, '-');
        System.out.println("Geben Sie einen Suchbegriff an!");
        System.out.print("Eingabe: ");
        String searchText = scanner.nextLine();
        printStrich(31, '-');
        List<Course> coursesList;
        try {
            coursesList = courseRepository.findAllCourseByNameOrDescription(searchText);
            for (Course c : coursesList) {
                System.out.println(c);
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbank fehler bei der Kurssuche: " + databaseException.getMessage());
        } catch (Exception e) {
            System.out.println("Unbekannter Fehler bei der Kurssuche " + e.getMessage());
        }
    }
```

\pagebreak

### deleteCourse

```java
  private void deleteCourse() {
        printStrich(33, '-');
        System.out.println("Welchen Kurs möchten Sie löschen?");
        System.out.print("Eingabe: ");
        Long courseIdToDelete = Long.parseLong(scanner.nextLine());
        printStrich(33, '-');
        try {
            courseRepository.deleteById(courseIdToDelete);
        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler beim löschen: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Unbekannter Fehler beim löschen: " + e.getMessage());
            printStrich(91, '!');
        }
    }
```

\pagebreak

### editCourseDetails

```java
private void editCourseDetails() {
        printStrich(54, '-');
        System.out.println("Für welche Kurs-ID möchten Sie die Kursdetails ändern?");
        System.out.print("Eingabe: ");
        Long courseId = Long.parseLong(scanner.nextLine());
        printStrich(54, '-');
        try {
            Optional<Course> courseOptional = courseRepository.getById(courseId);
            if (courseOptional.isEmpty()) {
                printStrich(50, '$');
                System.out.println("Kurs mit der ID [" + courseId + "] nicht in der Datenbank!");
                printStrich(50, '$');
            } else {
                Course course = courseOptional.get();
                System.out.println("Änderung für folgenden Kurs: ");
                System.out.println(course);
                printStrich(74, '*');
                String name, description, hours, dateFrom, dateTo, courseType;
                System.out.println("Bitte neue Kursdaten angaben (Enter,falls keine Änderungen gewünscht ist): ");

                System.out.print("Name: ");
                name = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Beschreibung: ");
                description = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Stundenanzahl: ");
                hours = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Startdatum (YYYY-MM-DD): ");
                dateFrom = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("Enddatum (YYYY-MM-DD): ");
                dateTo = scanner.nextLine();
                printStrich(20, '-');

                System.out.print("KursTyp (ZA/BF/FF/OE): ");
                courseType = scanner.nextLine();
                printStrich(20, '-');

                Optional<Course> optionalCourseUpdated = courseRepository.update(new Course(
                        course.getId(),
                        name.equals("") ? course.getName() : name,
                        description.equals("") ? course.getDescription() : description,
                        hours.equals("") ? course.getHours() : Integer.parseInt(hours),
                        dateFrom.equals("") ? course.getBeginDate() : Date.valueOf(dateFrom),
                        dateTo.equals("") ? course.getEndDate() : Date.valueOf(dateTo),
                        courseType.equals("") ? course.getCourseType() : CourseType.valueOf(courseType)
                ));
                optionalCourseUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Kurs aktualisiert: " + c),
                        () -> System.out.println("Kurs konnte nicht aktualisiert werden!"));
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            printStrich(44, '!');
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
            printStrich(44, '!');
        } catch (InvalidValueException invalidValueException) {
            printStrich(91, '!');
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
            printStrich(91, '!');
        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler bei Kursupdate: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Unbekannter Fehler bei Kursupdate: " + e.getMessage());
            printStrich(91, '!');
        }

    }
```

\pagebreak

### addCourse

```java
private void addCourse() {
        printStrich(30, '-');
        String name, description;
        int hours;
        Date dateFrom, dateTo;
        CourseType courseType;
        try {
            System.out.println("Bitte alle Kursdaten angeben: ");
            System.out.print("Name: ");
            name = scanner.nextLine();
            if (name.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            printStrich(35, '-');
            System.out.print("Beschreibung: ");
            description = scanner.nextLine();
            if (description.equals("")) throw new IllegalArgumentException("Eingabe darf nicht leer sein!");
            printStrich(35, '-');
            System.out.print("Stundenanzahl: ");
            hours = Integer.parseInt(scanner.nextLine());
            printStrich(35, '-');
            System.out.print("Startdatum (YYYY-MM-DD): ");
            dateFrom = Date.valueOf(scanner.nextLine());
            printStrich(35, '-');
            System.out.print("Enddatum (YYYY-MM-DD): ");
            dateTo = Date.valueOf(scanner.nextLine());
            printStrich(35, '-');
            System.out.print("KursTyp (ZA/BF/FF/OE): ");
            courseType = CourseType.valueOf(scanner.nextLine());
            printStrich(35, '-');
            Optional<Course> optionalCourse = courseRepository.insert(new Course(name, description, hours, dateFrom, dateTo, courseType));
            if (optionalCourse.isPresent()) {
                System.out.println("Kurs angelegt: " + optionalCourse.get());
            } else {
                System.out.println("Kurs kann nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            printStrich(44, '!');
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
            printStrich(44, '!');
        } catch (InvalidValueException invalidValueException) {
            printStrich(91, '!');
            System.out.println("Kursdaten nicht korrekt angegeben: " + invalidValueException.getMessage());
            printStrich(91, '!');
        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler beim einügen: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Unbekannter Fehler beim einfügen: " + e.getMessage());
            printStrich(91, '!');
        }
    }
```

\pagebreak

### showCourseDetails

```java
    private void showCourseDetails() {
        printStrich(55, '-');
        System.out.println("Für welchen Kurs möchten Sie die Kurs Details anzeigen?");
        System.out.print("Eingabe: ");
        Long courseId = Long.parseLong(scanner.nextLine());
        printStrich(55, '-');
        try {
            Optional<Course> courseOptional = courseRepository.getById(courseId);
            if (courseOptional.isPresent()) {
                System.out.println(courseOptional.get());
            } else {
                System.out.println("Kurs mit der ID [" + courseId + "] nicht gefunden!");
            }

        } catch (DatabaseException databaseException) {
            printStrich(91, '!');
            System.out.println("Datenbankfehler bei Kurs-Detailsanzeige: " + databaseException.getMessage());
            printStrich(91, '!');
        } catch (Exception e) {
            printStrich(91, '!');
            System.out.println("Ubnbekannter Fehler bei Kurs-Detailsanzeige: " + e.getMessage());
            printStrich(91, '!');
        }
    }
```

\pagebreak

### showAllCourses

```java
private void showAllCourses() {
        printStrich(54, '-');
        List<Course> courses = null;
        try {
            courses = courseRepository.getAll();
            if (courses.size() > 0) {
                for (Course course : courses) {
                    System.out.println(course);
                }
            } else {
                System.out.println("Kursliste leer!");
            }
        } catch (DatabaseException databaseException) {
            printStrich(90, '!');
            System.out.println("Datenbankfehler bei Anzeige aller Kurse: " + databaseException.getMessage());
            printStrich(90, '!');
        } catch (Exception e) {
            printStrich(90, '!');
            System.out.println("Unbekannter Fehler bei Anzeige aller Kurse: " + e.getMessage());
            printStrich(90, '!');
        }
    }
```

\pagebreak

### showCourseMenu

```java
    private void showCourseMenu() {
        printStrich(80, '=');
        System.out.println("\t\t\t\t\t\t\t\tKURSMANAGEMENT");
        System.out.println("(1) Kurs anlegen \t\t (2) Alle Kurse anzeigen \t (3) Kursdetails anzeigen ");
        System.out.println("(4) Kursdetails ändern \t (5) Kurs löschen \t\t\t (6) Kurssuche \t");
        System.out.println("(7) Laufende Kurse \t\t (-) ---- \t\t\t (-) ---- \t");
        System.out.println("\t\t\t\t\t\t (x) ENDE");
        printStrich(80, '=');
    }
```

\pagebreak

## Studenten

###

```java

```
