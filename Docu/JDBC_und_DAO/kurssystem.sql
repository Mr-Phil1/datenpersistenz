-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Generation Time: Mar 06, 2022 at 07:04 AM
-- Server version: 8.0.28
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kurssystem`
--
CREATE DATABASE IF NOT EXISTS `kurssystem` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `kurssystem`;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `hours` int NOT NULL,
  `begindate` date NOT NULL,
  `enddate` date NOT NULL,
  `coursetype` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `description`, `hours`, `begindate`, `enddate`, `coursetype`) VALUES
(1, 'POS1', 'Das ist die neue Beschreibung für den POS1-Kurs', 2, '2021-12-01', '2022-07-06', 'ZA'),
(2, 'Datamining for Beginners', 'Dieser Kurs beschäftigt sich mit Datamining und KI ...', 1, '2021-12-12', '2021-12-31', 'BF'),
(3, 'GameDev', 'Dieser Kurs beschäftigt sich mit Spieleentwicklung als Grundlage von ...', 2, '2021-05-05', '2021-05-20', 'BF'),
(5, 'Spass mit JDBC und DAO', 'Dieser Kurs beschäftigt sich mit der Einführung des DAO-Patterns mit JDBC.', 2, '2020-01-01', '2022-03-31', 'BF');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int NOT NULL,
  `vorname` varchar(200) NOT NULL,
  `nachname` varchar(200) NOT NULL,
  `geburtsdatum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `vorname`, `nachname`, `geburtsdatum`) VALUES
(1, 'Gunther', 'Müller', '2000-02-24'),
(2, 'Gunther', 'Maier', '2000-10-24'),
(4, 'test', 'test', '2000-09-08'),
(5, 'us', 'er', '2006-07-08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
